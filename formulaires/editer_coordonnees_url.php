<?php
/**
 * Gestion du formulaire d'édition d'une coordonnée adresse web
 *
 * @plugin     Coordonnees
 * @copyright  2014
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Definition des saisies du formulaire
 *
 * @param int|string $id_coordonnees_url
 *     Identifiant. 'new' pour une nouvelle adresse web.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse web créee à cet objet,
 *     tel que `article|3`
 * @return array
 *     Tableau des saisies
 */
function formulaires_editer_coordonnees_url_saisies_dist($id_coordonnees_url = 'new', $retour = '', $associer_objet = '') {
	$saisies = [
		// Titre
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				'label' => _T('coordonnees:label_titre'),
				'explication' => _T('coordonnees:explication_titre_coordonnee')
			]
		],
		// URL
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'url',
				'label' => _T('coordonnees:label_url_coordonnees_url'),
				'placeholder' => _T('coordonnees:placeholder_url_coordonnees_url'),
				'obligatoire' => 'oui'
			],
			// On vérifie juste qu'il y ait un protocole, peu importe lequel
			'verifier' => [
				'type' => 'url',
				'options' => [
					'type_protocole' => 'tous',
				],
			],
		],
		// Identifiant du compte si c'est un réseau social, optionnel
		// Nb : rempli automatiquement en post dans certains cas si laissé vide
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'identifiant',
				'label' => _T('coordonnees:label_identifiant_coordonnees_url'),
				'explication' => _T('coordonnees:explication_identifiant_coordonnees_url'),
			],
		],
	];

	// Type au début si on associe le site à un objet
	if ($associer_objet) {
		$saisie_type = [
			'saisie' => 'type_coordonnee',
			'options' => [
				'nom' => 'type',
				'label' => _T('coordonnees:label_type'),
				'coordonnee' => 'coordonnees_url',
				'class' => 'select2',
			],
		];
		$saisies = array_merge([$saisie_type], $saisies);
	}

	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_coordonnees_url
 *     Identifiant, 'new' pour une création
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier un objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_coordonnees_url_identifier_dist($id_coordonnees_url = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize([(int) $id_coordonnees_url, $associer_objet]);
}

/**
 * Chargement du formulaire d'édition d'une coordonnees_url
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_coordonnees_url
 *     Identifiant, 'new' pour une création
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier un objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_coordonnees_url_charger_dist($id_coordonnees_url = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('coordonnees_url', $id_coordonnees_url, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// valeur de la saisie "type" dans la table de liens
	if ($associer_objet) {
		[$objet, $id_objet] = explode('|', $associer_objet) ?: [];
		$id_coordonnees_url = (int) $id_coordonnees_url;
		$id_objet = (int) $id_objet;
		$valeurs['type'] = sql_getfetsel(
			'type',
			'spip_coordonnees_urls_liens',
			[
				'objet = ' . sql_quote($objet),
				"id_objet = $id_objet",
				"id_coordonnees_url = $id_coordonnees_url",
			]
		);
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition d'un coordonnees_url
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_coordonnees_url
 *     Identifiant, 'new' pour une création
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier un objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_coordonnees_url_verifier_dist($id_coordonnees_url = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	// verification generique
	$erreurs = formulaires_editer_objet_verifier('coordonnees_url', $id_coordonnees_url);

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition d'un coordonnees_url
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_coordonnees_url
 *     Identifiant, 'new' pour une création
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier un objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_coordonnees_url_traiter_dist($id_coordonnees_url = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$res = formulaires_editer_objet_traiter('coordonnees_url', $id_coordonnees_url, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if (
		$associer_objet
		and $id_coordonnees_url = (int) $res['id_coordonnees_url']
	) {
		// Associer l'objet
		[$objet, $id_objet] = explode('|', $associer_objet) ?: [];
		if (
			$objet
			and $id_objet = (int) $id_objet
		) {
			include_spip('action/editer_liens');
			objet_associer(
				['coordonnees_url' => $id_coordonnees_url],
				[$objet => $id_objet],
				['type' => _request('type')]
			);
			if ($redirect = $res['redirect'] ?? null) {
				$res['redirect'] = parametre_url($redirect, 'id_coordonnees_url', '', '&');
			}
		}
		// Recharger la liste des coordonnées liées
		$js = "<script type=\"text/javascript\">jQuery(document).ready(function($) { ajaxReload('coordonnees', {args:{coordonnee:'', id_coordonnee:''}, callback:()=>document.getElementById('liste-coordonnees_urls-lies').scrollIntoView({behavior:'smooth',block:'nearest'})}); });</script>";
		$res['message_ok'] .= $js;
	}

	return $res;
}
