<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');

function formulaires_configurer_coordonnees_saisies_dist() {
	$saisies = array(
		array(
			'saisie' => 'choisir_objets',
			'options' => array(
				'nom' => 'objets',
				'label' => _T('coordonnees:label_objets_actifs'),
				'explication' => _T('coordonnees:explication_objets_actifs'),
				'exclus' => array('spip_adresses', 'spip_numeros', 'spip_emails'),
				'defaut' => lire_config('coordonnees/objets'),
			),
		),
		array(
			'saisie' => 'checkbox',
			'options' => array(
				'nom' => 'adresses_champs_superflus',
				'label' => _T('coordonnees:configuration_adresses_champs_superflus_label'),
				'explication' => _T('coordonnees:configuration_adresses_champs_superflus_explication'),
				'data' => array(
					'voie' => _T('coordonnees:label_voie'),
					'complement' => _T('coordonnees:label_complement'),
					'boite_postale' => _T('coordonnees:label_boite_postale'),
					'code_postal' => _T('coordonnees:label_code_postal'),
					'ville' => _T('coordonnees:label_ville'),
					'zone_administrative' => _T('coordonnees:label_zone_administrative'),
					'pays' => _T('coordonnees:label_pays'),
				),
				'defaut' => lire_config('coordonnees/adresses_champs_superflus'),
			),
		),
	);
	
	// Si le plugin GIS est présent et que les points GIS sont activés sur les adresses
	if (test_plugin_actif('gis') and in_array('spip_adresses', lire_config('gis/gis_objets', array()))) {
		$saisies[] = array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'gis_synchro',
				'label' => 'Synchronisation avec GIS'
			),
			'saisies' => array(
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'gis[editer_point]',
						'label' => 'Éditer la géolocalisation',
						'label_case' => 'Ajouter l’édition de la géolocalisation durant l’édition d’une adresse',
						'explication' => 'La latitude, la longitude, et leur saisie cartographique seront ajoutées dans l’édition d’une adresse. Un point géolocalisé sera créé et lié à celle-ci.',
					),
				),
				array(
					'saisie' => 'case',
					'options' => array(
						'nom' => 'gis[synchroniser]',
						'label' => 'Synchronisation des liens',
						'label_case' => 'Synchroniser les adresses et les points géolocalisés',
						'explication' => 'Dès qu’une adresse est liée à un contenu, alors son point géolocalisé le sera aussi. De même lors d’un retrait.',
					),
				),
			),
		);
	}
	else {
		$saisies[] = array(
			'saisie' => 'explication',
			'options' => array(
				'nom' => 'gis_synchro',
				'texte' => 'Avec le plugin GIS et les points activés sur les adresses, il est possible de configurer des options de synchronisation.',
			),
		);
	}
	
	return $saisies;
}
