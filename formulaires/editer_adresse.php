<?php
/**
 * Gestion du formulaire de d'édition d'une adresse
 *
 * @plugin     Coordonnees
 * @copyright  2014
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Formulaires
 */
if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/actions');
include_spip('inc/editer');
include_spip('inc/config');
include_spip('inc/coordonnees');

/**
 * Definition des saisies du formulaire
 *
 * @param int|string $id_adresse
 *     Identifiant de l'adresse. 'new' pour une nouvelle adresse.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse créée à cet objet,
 *     tel que `article|3`
 * @return array
 *     Tableau des saisies
 */
function formulaires_editer_adresse_saisies_dist($id_adresse = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '', $obligatoire=false) {
	$champs_superflus = lire_config('coordonnees/adresses_champs_superflus', array());
	$identifiant = uniqid();
	
	// Si on connait le pays de l'adresse existante, on l'utilise, sinon défaut
	if (is_numeric($id_adresse) and $id_adresse>0 and $code_pays = sql_getfetsel('pays', 'spip_adresses', 'id_adresse = '.intval($id_adresse))) {
		$code_pays_defaut = $code_pays;
	}
	else {
		$code_pays_defaut = lire_config('pays/code_pays_defaut');
	}
	
	$saisies = array(
		array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'titre',
				'label' => _T('coordonnees:label_titre'),
				'placeholder' => _T('coordonnees:placeholder_titre_adresse')
			)
		),
	);
	
	// Si on a demandé à éditer un point géolocalisé
	if (test_plugin_actif('gis') and lire_config('coordonnees/gis/editer_point')) {
		include_spip('action/editer_liens');
		$liens = objet_trouver_liens(array('gis' => '*'), array('adresse' => $id_adresse));
		$id_gis = isset($liens[0]['id_gis']) ? $liens[0]['id_gis'] : 'new';
		$valeurs_gis = formulaires_editer_objet_charger('gis', $id_gis, '', '', $retour, '');
		
		$saisies[] = array(
			'saisie' => 'carte',
			'options' => array(
				'nom' => 'gis',
				'lat' => $valeurs_gis['lat'],
				'lon' => $valeurs_gis['lon'],
				'zoom' => $valeurs_gis['zoom'],
				// Soyons explicite pour mapper tous les champs entre GIS et Coordonnées
				'champ_adresse' => 'voie',
				'champ_pays' => 'prout', // Ne pas mettre le nom humain dans le champs pays des adresses qui attend un code !
				// 'champ_code_pays' => 'pays', // Pas pour l'instant, tant que Photon est pété et ne renvoie pas de code pays
				'champ_code_postal' => 'code_postal',
				'champ_ville' => 'ville',
				'champ_lat' => 'lat',
				'champ_lon' => 'lon',
			),
		);
		$saisies[] = array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'lat',
				'label' => _T('gis:lat'),
				'defaut' => $valeurs_gis['lat'],
			),
		);
		$saisies[] = array(
			'saisie' => 'input',
			'options' => array(
				'nom' => 'lon',
				'label' => _T('gis:lon'),
				'defaut' => $valeurs_gis['lon'],
			),
		);
		$saisies[] = array(
			'saisie' => 'hidden',
			'options' => array(
				'nom' => 'zoom',
				'label' => _T('gis:zoom'),
				'defaut' => $valeurs_gis['zoom'] ? $valeurs_gis['zoom'] : lire_config('gis/zoom'),
			),
			'verifier' => array(
				'type' => 'entier',
				'options' => array(
					'min' => 0,
				),
			),
		);
	}
	
	if (!in_array('pays', $champs_superflus)) {
		$saisies[] = array(
			'saisie' => 'pays',
			'options' => array(
				'nom' => 'pays',
				'label' => _T('coordonnees:label_pays'),
				'obligatoire' => $obligatoire,
				'class' => 'select2 chosen',
				'defaut' => lire_config('pays/code_pays_defaut'),
				'code_pays' => 'oui',
				'adresse-id' => $identifiant,
				'attributs' => 'data-adresse-id="'.$identifiant.'"',
			)
		);
	}
	
	// Par défaut, on va afficher les champs suivant le pays configuré par défaut, sinon la France pour reproduire comme avant
	$saisies_pays = coordonnees_adresses_saisies_par_pays($code_pays_defaut ? $code_pays_defaut : 'FR', $obligatoire);
	// On identifie cette adresse là pour pouvoir la manipuler indépendamment
	foreach ($saisies_pays as $cle=>$saisie) {
		$saisies_pays[$cle]['options']['adresse-id'] = $identifiant;
		$saisies_pays[$cle]['options']['attributs'] = 'data-adresse-id="'.$identifiant.'"';
	}
	
	$saisies = array_merge($saisies, $saisies_pays);
	
	//~ if (!in_array('voie', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'voie',
				//~ 'label' => _T('coordonnees:label_voie')
			//~ )
		//~ );
	//~ }
	
	//~ if (!in_array('complement', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'complement',
				//~ 'label' => _T('coordonnees:label_complement'),
				//~ 'placeholder' => _T('coordonnees:placeholder_complement_adresse')
			//~ )
		//~ );
	//~ }
	
	//~ if (!in_array('boite_postale', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'boite_postale',
				//~ 'label' => _T('coordonnees:label_boite_postale'),
			//~ )
		//~ );
	//~ }
	
	//~ if (!in_array('code_postal', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'code_postal',
				//~ 'label' => _T('coordonnees:label_code_postal')
			//~ ),
			//~ // decommenter ces lignes quand les codes postaux
			//~ // internationaux seront pris en compte par 'verifier'
			//~ /*'verifier' => array (
				//~ 'type' => 'code_postal'
			//~ )*/
		//~ );
	//~ }
	
	//~ if (!in_array('ville', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'ville',
				//~ 'label' => _T('coordonnees:label_ville')
			//~ )
		//~ );
	//~ }
	
	//~ if (!in_array('zone_administrative', $champs_superflus)) {
		//~ $saisies[] = array(
			//~ 'saisie' => 'input',
			//~ 'options' => array(
				//~ 'nom' => 'zone_administrative',
				//~ 'label' => _T('coordonnees:label_zone_administrative')
			//~ )
		//~ );
	//~ }
	
	// si on associe l'adresse à un objet, rajouter la saisie 'type'
	if ($associer_objet) {
		$saisie_type = [
			'saisie' => 'type_coordonnee',
			'options' => [
				'nom' => 'type',
				'label' => _T('coordonnees:label_type'),
				'coordonnee' => 'adresse',
				'class' => 'select2',
			],
		];
		$saisies = array_merge([$saisie_type], $saisies);
	}
	
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_adresse
 *     Identifiant de l'adresse. 'new' pour une nouvelle adresse.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse créée à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une adresse source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'adresse, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_adresse_identifier_dist($id_adresse = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '', $obligatoire=false) {
	return serialize(array(intval($id_adresse), $associer_objet));
}

/**
 * Chargement du formulaire d'édition d'une adresse
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_adresse
 *     Identifiant de l'adresse. 'new' pour une nouvelle adresse.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse créée à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une adresse source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'adresse, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_adresse_charger_dist($id_adresse = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '', $obligatoire=false) {
	$valeurs = formulaires_editer_objet_charger('adresse', $id_adresse, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// valeur de la saisie "type" dans la table de liens
	if ($associer_objet) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		$valeurs['type'] = sql_getfetsel('type', 'spip_adresses_liens', 'objet='.sql_quote($objet).' AND id_objet='.intval($id_objet).' AND id_adresse='.intval($id_adresse));
	}

	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition d'une adresse
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_adresse
 *     Identifiant de l'adresse. 'new' pour une nouvelle adresse.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse créée à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une adresse source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'adresse, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_adresse_verifier_dist($id_adresse = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '', $obligatoire=false) {
	// verification generique
	$erreurs = formulaires_editer_objet_verifier('adresse', $id_adresse);

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition d'une adresse
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_adresse
 *     Identifiant de l'adresse. 'new' pour une nouvelle adresse.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param string $associer_objet
 *     Éventuel `objet|x` indiquant de lier l'adresse créée à cet objet,
 *     tel que `article|3`
 * @param int $lier_trad
 *     Identifiant éventuel d'une adresse source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'adresse, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_adresse_traiter_dist($id_adresse = 'new', $retour = '', $associer_objet = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '', $obligatoire=false) {
	$res = formulaires_editer_objet_traiter('adresse', $id_adresse, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

	// Un lien a prendre en compte ?
	if ($associer_objet and $id_adresse = $res['id_adresse']) {
		list($objet, $id_objet) = explode('|', $associer_objet);
		if ($objet and $id_objet == intval($id_objet)) {
			include_spip('action/editer_liens');
			objet_associer(array('adresse' => $id_adresse), array($objet => $id_objet), array('type' => _request('type')));
			if (isset($res['redirect'])) {
				$res['redirect'] = parametre_url($res['redirect'], 'id_adresse', '', '&');
			}
			// Recharger la liste des coordonnées liées
			$js = "<script type=\"text/javascript\">jQuery(document).ready(function($) { ajaxReload('coordonnees', {args:{coordonnee:'', id_coordonnee:''}, callback:()=>document.getElementById('liste-adresses-lies').scrollIntoView({behavior:'smooth',block:'nearest'})}); });</script>";
			$res['message_ok'] .= $js;
		}
	}

	return $res;
}
