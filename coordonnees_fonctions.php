<?php
/**
 * Fonctions utiles au plugin Coordonnées
 *
 * @plugin     Coordonnees
 * @copyright  2013
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Lister les types de liaisons des coordonnées, ou un type en particulier.
 *
 * 2 utilisations principales :
 * - Donner les types pour la saisie `type_coordonnee`
 * - Trouver le label d'un type : balise `#TYPE_COORDONNEE`
 *
 * @note
 * Certains types peuvent être des sous-groupes, par exemple `sociaux` pour les sites internet.
 * Si on demande ce type, cela renvoie donc un tableau.
 *
 * @note
 * Références :
 * adresses : RFC2426/CCITT.X520
 * numéros :  RFC2426/CCITT.X500
 *            RFC6350/CCITT.X520.1988
 * emails :   RFC2426/IANA
 *            RFC6350/CCITT.X520+RFC5322
 * sites :    RFC6350 + custom
 * 
 * @example
 * ```
 * coordonnees_lister_types_coordonnees();                             // Tableau des types de toutes les coordonnées
 * coordonnees_lister_types_coordonnees('coordonnees_url');            // Tableau des types d'une coordonnée
 * coordonnees_lister_types_coordonnees('coordonnees_url', 'sociaux'); // Tableau des types d'un sous-groupe d'une coordonnée
 * coordonnees_lister_types_coordonnees('coordonnees_url', 'home');    // Label du type `home` d'une coordonnée
 * ```
 *
 * @uses pipeline `types_coordonnees`
 *
 * @param string|null $coordonnee
 *     Type de coordonnée : adresse | numero | email | coordonnees_url
 *     déprécié : adr | tel | mel
 * @param string|null $type
 *     Le type d'une coordonnée : home, work, etc.
 *     Si le type est un sous-groupe, ça retourne un tableau.
 * @return array|string
 *     - Sans paramètre : tableau des couples type/label de toutes les coordonnées
 *       array<string, array<string, string|array>>
 *     - Avec `$coordonnee` : tableau des couples type/label de la coordonnée
 *       Si coordonnée inexistante, tableau vide
 *       array<string, string|array>
 *     - Avec `$coordonnee` et `$type` :
 *       - si c'est un sous-groupe : tableau des couples type/label
 *         array<string, string>
 *       - sinon le label du type
 *         Si type inexistant, valeur retournée telle quelle
 */
function coordonnees_lister_types_coordonnees(?string $coordonnee = null, ?string $type = null) {

	if ($coordonnee) {
		include_spip('base/objets');
		$coordonnee = objet_type($coordonnee);
	}

	// Correspondance avec les abbréviations utilisées dans les chaînes de langue (pfff…)
	$lang2coordonnee = [
		'adr' => 'adresse',
		'tel' => 'numero',
	];
	$coordonnee2lang = array_flip($lang2coordonnee);

	// Ne faire qu'une seule fois la liste complète
	static $liste;
	if (is_null($liste)) {
		$liste = [];

		// Liste initiales des types
		$types = [
			'adresse' => ['work', 'home', 'pref', 'postal', 'dom', 'intl', 'parcel'],
			'numero'  => ['voice', 'work', 'home', 'msg', 'pref', 'fax', 'cell', 'dsl', 'video', 'pager', 'bbs', 'modem', 'car', 'isdn', 'pcs'],
			'email'   => ['work', 'home', 'internet', 'pref', 'x400'],
			'coordonnees_url' => [
				'work', 'home', 'school', 'blog', 'pref', 'other',
				// Réseaux sociaux dans un sous-groupe
				'sociaux' => [
					'facebook', 'twitter', 'seenthis', 'instagram', 'linkedin', 'mastodon', 'youtube', 'peertube', 'vimeo',
				],
			],
		];

		// Ajouter les chaînes de langue
		// Attention : pour les sous-groupes, on a besoin des labels séparément.
		// On les place dans la clé `labels_groupes` à la racine.
		foreach ($types as $_coordonnee => $_types) {
			foreach ($_types as $k => $_type) {
				$coordonnee_lang = $coordonnee2lang[$_coordonnee] ?? $_coordonnee;
				// Soit c'est direct une chaîne de langue
				if (is_string($_type)) {
					$liste[$_coordonnee][$_type] = _T("coordonnees:type_{$coordonnee_lang}_{$_type}");
				// Soit c'est un sous-groupe
				} elseif (is_array($_type)) {
					foreach ($_type as $_type_groupe) {
						$liste[$_coordonnee][$k][$_type_groupe] = _T("coordonnees:type_{$coordonnee_lang}_{$_type_groupe}");
					}
					// On note le label à part
					$liste['labels_groupes'][$k] = _T("coordonnees:type_{$coordonnee_lang}_{$k}");
				}
			}
		}

		// Envoyer aux plugins pour qu'ils complètent ou altèrent la liste
		$liste = pipeline('types_coordonnees', $liste);
	}

	// Soit toute la liste (array)
	if ($coordonnee === null) {
		$retour = $liste;
	// Soit tous les types d'une coordonnée (array)
	} elseif ($type === null) {
		$retour = $liste[$coordonnee] ?? [];
	// soit tous les types d'un sous-groupe (array)
	} elseif (isset($liste[$coordonnee][$type])) {
		$retour = $liste[$coordonnee][$type];
	// Soit un type précis (string)
	} else {
		// Tout mettre à plat (il peut y avoir des sous-groupes)
		$types = [];
		foreach ($liste[$coordonnee] ?? [] as $_type => $label) {
			if (is_array($label)) {
				$types = array_merge($types, $label);
			} else {
				$types[$_type] = $label;
			}
		}
		$retour = $types[$type] ?? $type;
	}

	return $retour;
}

/**
 * Affichage du type de liaison d'une coordonnée
 *
 * Fonction privee mutualisée utilisée par les filtres logo_type_xx
 *
 * @note
 * Nomenclature des fichiers d'images :
 *
 * - avec le paramètre `$coordonnee` : `type_{$coordonnee}_{$type}.ext`
 *   ex: `type_adresse_home.svg`
 * - sans le paramètre `$coordonnee` : `type_{$type}.ext`
 *   ex: `type_home.svg`
 *
 * @note
 * http://www.alsacreations.com/tuto/lire/1222-microformats-design-patterns.html
 * http://www.alsacreations.com/tuto/lire/1223-microformats-composes.html
 *
 * @param string $coordonnee
 *     Suffixe du du filtre logo_type_{$coordonnee} appelant ;
 *     Infixe du logo "type_{$coordonnee}_{$type}.???" correspondant ;
 *     adresse | numero | email
 *     à éviter : adr | tel | mel
 * @param string $type
 *     Valeur associée transmise par le filtre logo_type_{$coordonnee} ;
 *     Suffixe du logo "type_{$coordonnee}_{$type}.???" correspondant ;
 *     Correspond au "type" de liaison de la la coordonnée (home, work, etc.)
 * @return string
 *     Balise `<IMG>` ou `<ABBR>` (sinon),
 *     avec classes semantiques micro-format et traduction des valeurs clés RFC2426
 */
function logo_type_($coordonnee = '', $type = '') {
	static $formats_logos;

	include_spip('inc/utils');
	include_spip('inc/filtres');
	if (is_null($formats_logos)) {
		$formats_logos = $GLOBALS['formats_logos'];
		$formats_logos = array_diff($formats_logos, ['svg']);
		array_unshift($formats_logos, 'svg');
	}

	// chaîne de langue
	$type = strtolower($type);
	$langue_coordonnee = coordonnees_lister_types_coordonnees($coordonnee, $type);
	$langue_perso = _T("perso:type_{$type}", '', array('force' => false));
	$langue = ($type ? ($coordonnee ? $langue_coordonnee : $langue_perso ) : '');

	// fichier image
	$cherche = [];
	if ($coordonnee){
		$cherche[] = "type_{$coordonnee}_{$type}";
		$cherche[] = "type_{$coordonnee}";
	} else {
		$cherche[] = "type_{$type}";
	}

	foreach ($cherche as $filename) {
		foreach ($formats_logos as $ext) {
			if ($image = chemin_image($filename.'.'.$ext)) {
				break 2;
			}
		}
	}

	if($langue){
		$balise_img = chercher_filtre('balise_img');
		if (isset($image))
			return inserer_attribut($balise_img($image, $type, "picto_type_coordonnees picto_type_".$coordonnee), 'title', $langue);
		elseif ($type)
			return inserer_attribut(inserer_attribut(wrap($langue, '<abbr>'), 'title', $type), 'class', 'type');
		else
			return '';
	} else
		return '';
}

/**
 * Filtre d'affichage du type d'une adresse
 *
 * @uses logo_type_()
 * @filtre
 *
 * @param string $type_adresse
 *     Valeur du type de liaison (cf. logo_type_).
 *     Les valeurs nativement prises en compte sont les codes normalisés
 *     CCITT.X520/RFC2426 (section 3.2.1) : dom home intl parcel postal pref work
 * @return string
 *     Balise HTML micro-format (cf. logo_type_)
 */
function filtre_logo_type_adresse($type_adresse) {
	return logo_type_('adresse', $type_adresse);
}

/**
 * Filtre d'affichage du type d'un numero
 *
 * @uses logo_type_()
 * @filtre
 *
 * @param string $type_numero
 *     Valeur du type de liaison (cf. logo_type_).
 *     Les valeurs nativement prises en compte sont les codes normalisés
 *     CCITT.X500/RFC2426 (section 3.3.1) :      bbs car cell fax home isdn modem msg pager pcs pref video voice work
 *     CCITT.X520.1988/RFC6350 (section 6.4.1) : cell fax pager text textphone video voice x-... (iana-token)
 *     ainsi que :                               dsl
 *     (<http://fr.wikipedia.org/wiki/Digital_Subscriber_Line#Familles>)
 * @return string
 *     Balise HTML micro-format (cf. logo_type_)
 */
function filtre_logo_type_numero($type_numero) {
	return logo_type_('numero', $type_numero);
}

/**
 * Filtre d'affichage du type d'un courriel
 *
 * @uses logo_type_()
 * @filtre
 *
 * @param string $type_email
 *     Valeur du type de liaison (cf. logo_type_).
 *     Les valeurs nativement prises en compte sont les codes normalisés
 *     IANA/RFC2426 (section 3.3.2) :               internet pref x400
 *     CCITT.X520+RFC5322/RFC6350 (section 6.4.2) : home (perso) intl work (pro)
 * @return string
 *     Balise HTML micro-format (cf. logo_type_)
**/
function filtre_logo_type_email($type_email) {
	return logo_type_('email', $type_email);
}

/**
 * Compile la balise #REGION en faisant une exception pour la boucle ADRESSES
 *
 * @param $p
 * @return
 * 		Code PHP à exécuter
 */
if (!function_exists('balise_REGION_dist')) { // Si pas déjà défini dans un autre plugin
function balise_REGION_dist($p) {
	// Si le champ existe on l'utilise en priorité
	if (
		$region = champ_sql('region', $p, false)
		and $region != '@$Pile[0][\'region\']'
	) {
		$p->code = "safehtml($region)";
		// $p->interdire_scripts = true;
	}
	// Si le champ n'existe pas, on cherche le champ "zone_administrative" à la place
	else {
		$region = champ_sql('zone_administrative', $p, false);
		$p->code = "safehtml($region)";
	}

	return $p;
}
}

/**
 * Formate une adresse selon le standard du pays
 */
function coordonnees_adresses_formater($id_ou_adresse, $html=true) {
	$formatage = '';
	$html = (bool)$html;

	if (
		(is_array($id_ou_adresse) and $adresse = $id_ou_adresse)
		or
		(
			$id_adresse = intval($id_ou_adresse)
			and include_spip('base/abstract_sql')
			and $adresse = sql_fetsel('*', 'spip_adresses', 'id_adresse = '.$id_adresse)
		)
	) {
		include_spip('inc/coordonnees');
		include_spip('inc/config');
		coordonnees_loader();

		$addressFormatRepository = new CommerceGuys\Addressing\AddressFormat\AddressFormatRepository();
		$countryRepository = new CommerceGuys\Addressing\Country\CountryRepository();
		$subdivisionRepository = new CommerceGuys\Addressing\Subdivision\SubdivisionRepository();
		$formatter = new CommerceGuys\Addressing\Formatter\DefaultFormatter($addressFormatRepository, $countryRepository, $subdivisionRepository);
		$address = new CommerceGuys\Addressing\Address();
		$address = $address
			->withCountryCode(isset($adresse['pays']) && strlen($adresse['pays']) ? $adresse['pays'] : lire_config('pays/code_pays_defaut', 'FR'))
			->withAdministrativeArea($adresse['zone_administrative'] ?? '')
			->withLocality($adresse['ville'] ?? '')
			->withDependentLocality($adresse['localite_dependante'] ?? '')
			->withAddressLine1($adresse['voie'] ?? '')
			->withAddressLine2($adresse['complement'] ?? '')
			->withPostalCode($adresse['code_postal'] ?? '');

		$formatage = $formatter->format(
			$address,
			array(
				'locale'=>$GLOBALS['spip_lang'],
				'html' => $html,
				'html_tag' => 'address',
				'html_attributes' => array(
					'translate' => 'no',
				)
			)
		);
	}

	return $formatage;
}

/**
 * Compile la balise #ADRESSE_FORMATER qui affiche comme il faut suivant le pays
 *
 * À utiliser dans une boucle ADRESSES ou avec #ADRESSE_FORMATER{#ID_ADRESSE}
 *
 * @param $p
 * @return
 * 		Code PHP à exécuter
 */
function balise_ADRESSE_FORMATER_dist($p) {
	$_id = interprete_argument_balise(1, $p);

	if (!$_id) {
		$_id = champ_sql('id_adresse', $p);
	}

	$p->code = "coordonnees_adresses_formater($_id)";

	return $p;
}

/**
 * Compile la balise #TYPE_COORDONNEE qui retourne le label du type d'une coordonnée.
 *
 * Utilisation :
 * - soit dans une boucle coordonnée, sans paramètre.
 * - soit hors boucle avec 2 params coordonnee et type.
 *
 * À utiliser en alternative à la balise #TYPE, qui retourne la valeur brute.
 *
 * @example
 * ```
 * <BOUCLE(EMAILS) {id_auteur}>
 * #TYPE_COORDONNEE
 * </BOUCLE>
 * ```
 *
 * @example
 * ```
 * #TYPE_COORDONNEE{email, work}
 * ```
 * 
 * @note
 * On pourrait appliquer un traitement auto à la balise #TYPE,
 * mais ça casserait son utilisation éventuelle sans étoile dans les squelettes déjà dans la nature.
 * Dans une version x+1 ?
 *
 * @uses coordonnees_lister_types_coordonnees()
 *
 * @param Spip\Compilateur\Noeud\Champ $p
 *   Pile au niveau de la balise
 * @return Spip\Compilateur\Noeud\Champ
 *   Pile complétée par le code à générer
 */
function balise_TYPE_COORDONNEE_dist($p) {

	// En priorité en prend les 2 params
	$_objet = interprete_argument_balise(1, $p);
	$_type = interprete_argument_balise(2, $p);

	// Sans param on prend les valeurs de la boucle
	if (
		!$_objet
		and !$_type
		and $index = index_boucle($p)
	) {
		$_objet = $p->boucles[$index]->type_requete ?? null;
		$_objet = ($_objet ? "'$_objet'" : null);
		$_type = champ_sql('type', $p);
	}

	if (
		$_objet
		and $_type
	) {
		$p->code = "coordonnees_lister_types_coordonnees($_objet, $_type)";
	} else {
		$p->code ="''";
	}

	return $p;
}

/**
 * Fonction privée qui renvoie les types d'une coordonnée, utilisables en data des saisies.
 *
 * On remplace les clés des sous-groupes par leurs vrais labels, pour le <optgroup>
 *
 * @uses coordonnees_lister_types_coordonnees()
 *
 * @param array $types
 * @return array
 */
function coordonnees_data_types_coordonnee(string $coordonnee): array {
	$liste = coordonnees_lister_types_coordonnees();
	$types = $liste[$coordonnee] ?? [];
	foreach ($types as $k => $type) {
		if (
			is_array($type)
			and $label_groupe = $liste['labels_groupes'][$k] ?? null
		) {
			unset($types[$k]);
			$types[$label_groupe] = $type;
		}
	}

	return $types;
}


// ====================
// FONCTIONS DÉPRÉCIÉES
// ====================


/**
 * Filtre renvoyant les types d'adresses
 *
 * @deprecated v4.2.0
 */
function filtre_coordonnees_lister_types_adresses(?string $type = null) {
	return coordonnees_lister_types_coordonnees('adresse', $type);
}

/**
 * Filtre renvoyant les types de numéro
 *
 * @deprecated v4.2.0
 */
function filtre_coordonnees_lister_types_numeros(?string $type = null) {
	return coordonnees_lister_types_coordonnees('numero', $type);
}

/**
 * Filtre renvoyant les types d'emails
 *
 * @deprecated v4.2.0
 */
function filtre_coordonnees_lister_types_emails(?string $type = null) {
	return coordonnees_lister_types_coordonnees('email', $type);
}