<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/coordonnees?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Endereço',
	'adresse_champ_code_postal_eircode_label' => 'Código Postal',
	'adresse_champ_code_postal_pin_label' => 'PIN',
	'adresse_champ_code_postal_zip_label' => 'ZIP',
	'adresse_champ_localite_dependante_district_label' => 'Distrito',
	'adresse_champ_localite_dependante_neighborhood_label' => 'Bairro',
	'adresse_champ_localite_dependante_suburb_label' => 'Periferia urbana',
	'adresse_champ_localite_dependante_townland_label' => 'Townland',
	'adresse_champ_localite_dependante_village_township_label' => 'Conselho de aldeia',
	'adresse_champ_ville_district_label' => 'Distrito',
	'adresse_champ_ville_post_town_label' => 'Município',
	'adresse_champ_ville_suburb_label' => 'Periferia urbana',
	'adresse_champ_zone_administrative_area_label' => 'Zona',
	'adresse_champ_zone_administrative_canton_label' => 'Cantão',
	'adresse_champ_zone_administrative_county_label' => 'Condado',
	'adresse_champ_zone_administrative_department_label' => 'Região',
	'adresse_champ_zone_administrative_district_label' => 'Distrito',
	'adresse_champ_zone_administrative_do_si_label' => 'Do Si',
	'adresse_champ_zone_administrative_emirate_label' => 'Emirado',
	'adresse_champ_zone_administrative_island_label' => 'Ilha',
	'adresse_champ_zone_administrative_oblast_label' => 'Oblast',
	'adresse_champ_zone_administrative_parish_label' => 'Paróquia',
	'adresse_champ_zone_administrative_prefecture_label' => 'Prefeitura',
	'adresse_champ_zone_administrative_province_label' => 'Província',
	'adresse_champ_zone_administrative_state_label' => 'Estado',
	'adresse_perso' => 'Domicílio',
	'adresse_pro' => 'Profissional',
	'adresses' => 'Endereços',
	'ajouter_adresse' => 'Adicionar um endereço',
	'ajouter_email' => 'Adicionar um e-mail', # MODIF
	'ajouter_numero' => 'Adicionar um número', # MODIF
	'ajouter_telephone' => 'Adicionar um número', # MODIF

	// B
	'bouton_dissocier' => 'Remover',
	'bouton_dissocier_adresse' => 'Remover este endereço',
	'bouton_dissocier_email' => 'Remover este e-mail', # MODIF
	'bouton_dissocier_numero' => 'Remover este número',

	// C
	'configuration_adresses_champs_superflus_explication' => 'Permite-lhe omitir certos campos nos endereços, que não são relevantes para este sítio.',
	'configuration_adresses_champs_superflus_label' => 'Campos desnecessários dos endereços',
	'configuration_coordonnees' => 'Configuração de dados para contacto',
	'confirmer_suppression_adresse' => 'Quer mesmo apagar este endereço?',
	'confirmer_suppression_email' => 'Quer mesmo apagar este e-mail?',
	'confirmer_suppression_numero' => 'Quer mesmo apagar este número?', # MODIF
	'contacter_adresse' => 'Contactar por e-mail',
	'contacter_adresse_qui' => 'Contactar @nom@ por e-mail',
	'contacter_email' => 'Contactar por e-mail', # MODIF
	'contacter_email_qui' => 'Contactar @nom@ por e-mail', # MODIF
	'contacter_telephone' => 'Contactar por telefone',
	'contacter_telephone_qui' => 'Contactar @nom@ por telefone',

	// E
	'editer_adresse' => 'Editar um endereço',
	'editer_email' => 'Editar um e-mail',
	'editer_numero' => 'Editar um número',
	'email' => 'E-mail',
	'emails' => 'E-mails',
	'explication_objets_actifs' => 'Em que objectos editoriais devem ser oferecidos os dados de contacto?',
	'explication_type_email' => 'O tipo de e-mail pode ser "pessoal" ou "profissional".',

	// I
	'info_1_adresse' => '1 endereço',
	'info_1_email' => '1 e-mail',
	'info_1_numero' => '1 número', # MODIF
	'info_aucun_email' => 'Nenhum e-mail',
	'info_aucun_numero' => 'Nenhum número', # MODIF
	'info_aucune_adresse' => 'Nenhum endereço',
	'info_gauche_numero_adresse' => 'Endereço Nº',
	'info_gauche_numero_email' => 'E-mail N°', # MODIF
	'info_gauche_numero_numero' => 'Número Nº', # MODIF
	'info_nb_adresses' => '@nb@ endereços',
	'info_nb_emails' => '@nb@ E-mails',
	'info_nb_numeros' => '@nb@ números', # MODIF
	'item_nouveau_numero' => 'Novo número', # MODIF
	'item_nouvel_email' => 'Novo e-mail',
	'item_nouvelle_adresse' => 'Novo endereço',

	// L
	'label_boite_postale' => 'Caixa Postal',
	'label_code_postal' => 'Código Postal',
	'label_complement' => 'Complemento de morada',
	'label_email' => 'E-mail',
	'label_localite_dependante' => 'Localidade dependente',
	'label_numero' => 'Número',
	'label_objets_actifs' => 'Objectos',
	'label_pays' => 'País',
	'label_telephone' => 'Telefone',
	'label_titre' => 'Título',
	'label_type' => 'Tipo',
	'label_type_adresse' => 'Tipo de endereço',
	'label_type_email' => 'Tipo de e-mail',
	'label_type_numero' => 'Tipo de número',
	'label_ville' => 'Cidade',
	'label_voie' => 'Rua, nº...',
	'label_zone_administrative' => 'Zona administrativa',
	'logo_adresse' => 'Logotipo do endereço',
	'logo_email' => 'Logotipo do e-mail',
	'logo_numero' => 'Logotipo do número', # MODIF

	// M
	'modifier_adresse' => 'Modificar este endereço',
	'modifier_email' => 'Modificar este e-mail',
	'modifier_numero' => 'Modifier este número',

	// N
	'nouveau_numero' => 'Novo número',
	'nouvel_email' => 'Novo e-mail',
	'nouvelle_adresse' => 'Novo endereço',
	'numero' => 'Número',
	'numeros' => 'Números',

	// P
	'placeholder_complement_adresse' => 'andar, nº da porta...',
	'placeholder_email' => 'email@site.com',
	'placeholder_titre_adresse' => 'Um nome para identificar o endereço',
	'placeholder_titre_email' => 'Um nome para identificar o e-mail', # MODIF
	'placeholder_titre_numero' => 'Um nome para identificar o número de telefone', # MODIF

	// S
	'supprimer_adresse' => 'Remover este endereço',
	'supprimer_email' => 'Remover este e-mail',
	'supprimer_numero' => 'Remover este número',

	// T
	'titre_coordonnees' => 'Dados para contacto',
	'type_adr_dom' => 'Residencial',
	'type_adr_home' => 'Pessoal',
	'type_adr_intl' => 'Estrangeiro',
	'type_adr_parcel' => 'Parcela',
	'type_adr_postal' => 'Postal (em posta restante)',
	'type_adr_pref' => 'Principal', # MODIF
	'type_adr_work' => 'Profissional',
	'type_coordonnees_url_home' => 'Domicílio',
	'type_email_home' => 'Pessoal',
	'type_email_internet' => 'Internet',
	'type_email_intl' => 'Internacional',
	'type_email_pref' => 'Preferido',
	'type_email_work' => 'Profissional',
	'type_email_x400' => 'X.400',
	'type_mel_home' => 'Pessoal',
	'type_mel_intl' => 'Internacional',
	'type_mel_work' => 'Profissional',
	'type_tel_bbs' => 'Serviço de mensagens',
	'type_tel_car' => 'Viatura',
	'type_tel_cell' => 'Portátil',
	'type_tel_dsl' => 'box ADSL',
	'type_tel_fax' => 'Fax',
	'type_tel_home' => 'Residência',
	'type_tel_isdn' => 'RDSI (Rede Digital de Serviços Integrados)',
	'type_tel_modem' => 'MoDem',
	'type_tel_msg' => 'Correio de voz (atendedor de chamadas)',
	'type_tel_pager' => 'Pager (bip)',
	'type_tel_pcs' => 'Serviço de comunicação pessoal',
	'type_tel_pref' => 'Favorito', # MODIF
	'type_tel_text' => 'Mensagem de texto',
	'type_tel_textphone' => 'Transcrição de texto',
	'type_tel_video' => 'Videofone (videoconferência)',
	'type_tel_voice' => 'Vocal',
	'type_tel_work' => 'Profissional'
);
