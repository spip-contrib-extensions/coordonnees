<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/coordonnees?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => '住所',
	'adresse_champ_code_postal_eircode_label' => 'Eircode',
	'adresse_champ_code_postal_pin_label' => 'PIN',
	'adresse_champ_code_postal_zip_label' => 'ZIP',
	'adresse_champ_localite_dependante_district_label' => '地区',
	'adresse_champ_localite_dependante_neighborhood_label' => '区',
	'adresse_champ_localite_dependante_suburb_label' => '郊外',
	'adresse_champ_localite_dependante_townland_label' => '街区',
	'adresse_champ_localite_dependante_village_township_label' => '村',
	'adresse_champ_ville_district_label' => '地区',
	'adresse_champ_ville_post_town_label' => '市町村',
	'adresse_champ_ville_suburb_label' => '郊外',
	'adresse_champ_zone_administrative_area_label' => '圏',
	'adresse_champ_zone_administrative_canton_label' => '州',
	'adresse_champ_zone_administrative_county_label' => '州',
	'adresse_champ_zone_administrative_department_label' => '県',
	'adresse_champ_zone_administrative_district_label' => '地方',
	'adresse_champ_zone_administrative_do_si_label' => '県',
	'adresse_champ_zone_administrative_emirate_label' => '首長国',
	'adresse_champ_zone_administrative_island_label' => '島',
	'adresse_champ_zone_administrative_oblast_label' => '州',
	'adresse_champ_zone_administrative_parish_label' => '区',
	'adresse_champ_zone_administrative_prefecture_label' => '都道府県',
	'adresse_champ_zone_administrative_province_label' => '州',
	'adresse_champ_zone_administrative_state_label' => '州',
	'adresse_perso' => '住所',
	'adresse_pro' => '職業',
	'adresses' => '住所',
	'ajouter_adresse' => '住所を追加する',
	'ajouter_email' => 'Eメールを追加する', # MODIF
	'ajouter_numero' => '番号を追加する', # MODIF
	'ajouter_telephone' => '番号を追加する', # MODIF

	// B
	'bouton_dissocier' => '取り消す',
	'bouton_dissocier_adresse' => 'この住所を取り消す',
	'bouton_dissocier_email' => 'このメールアドレスを取り消す', # MODIF
	'bouton_dissocier_numero' => 'この電話番号を取り消す',

	// C
	'configuration_adresses_champs_superflus_explication' => 'このサイトでは不要なアドレス欄を使わないようにする',
	'configuration_adresses_champs_superflus_label' => '不要なアドレス欄',
	'configuration_coordonnees' => '連絡先の設定',
	'confirmer_suppression_adresse' => 'この住所を本当に削除しますか？',
	'confirmer_suppression_email' => 'このメールを本当に削除しますか？',
	'confirmer_suppression_numero' => 'この番号を本当に削除しますか？', # MODIF
	'contacter_adresse' => '郵便で連絡する',
	'contacter_adresse_qui' => '@nom@ に郵便で連絡する',
	'contacter_email' => 'メールで連絡する', # MODIF
	'contacter_email_qui' => '@nom@ にメールで連絡する', # MODIF
	'contacter_telephone' => '電話で連絡する',
	'contacter_telephone_qui' => '@nom@ に電話で連絡する',

	// E
	'editer_adresse' => '住所を編集する',
	'editer_email' => 'メールアドレスを編集する',
	'editer_numero' => '電話番号を編集する',
	'email' => 'メール',
	'emails' => 'メール',
	'explication_objets_actifs' => 'この連絡先をどの編集対象に使いますか?', # RELIRE
	'explication_type_email' => '’個人用’ か ’仕事用’を選んでください',

	// I
	'info_1_adresse' => '住所',
	'info_1_email' => 'メールアドレス',
	'info_1_numero' => '電話番号', # MODIF
	'info_aucun_email' => 'メールアドレスはありません',
	'info_aucun_numero' => '電話番号はありません', # MODIF
	'info_aucune_adresse' => '住所はありません',
	'info_gauche_numero_adresse' => '住所 N°',
	'info_gauche_numero_email' => 'メール N°', # MODIF
	'info_gauche_numero_numero' => '電話番号 N°', # MODIF
	'info_nb_adresses' => '@nb@ 住所',
	'info_nb_emails' => '@nb@ メール',
	'info_nb_numeros' => '@nb@ 電話番号', # MODIF
	'item_nouveau_numero' => '新しい電話番号', # MODIF
	'item_nouvel_email' => '新しいメールアドレス',
	'item_nouvelle_adresse' => '新しい住所',

	// L
	'label_boite_postale' => '郵便箱',
	'label_code_postal' => '郵便番号',
	'label_complement' => '建物名・号室',
	'label_email' => 'メール',
	'label_localite_dependante' => '付属地域', # RELIRE
	'label_numero' => '電話番号',
	'label_objets_actifs' => '対象', # RELIRE
	'label_pays' => '国',
	'label_telephone' => '電話',
	'label_titre' => 'タイトル',
	'label_type' => 'タイプ',
	'label_type_adresse' => '住所のタイプ',
	'label_type_email' => 'メールアドレスのタイプ',
	'label_type_numero' => '電話番号のタイプ',
	'label_ville' => '市',
	'label_voie' => '番地',
	'label_zone_administrative' => '行政区域',
	'logo_adresse' => '住所のロゴ',
	'logo_email' => 'メールアドレスのロゴ',
	'logo_numero' => '電話番号のロゴ', # MODIF

	// M
	'modifier_adresse' => 'この住所を修正する',
	'modifier_email' => 'このメールアドレスを修正する',
	'modifier_numero' => 'この電話番号を修正する',

	// N
	'nouveau_numero' => '新しい電話番号',
	'nouvel_email' => '新しいメールアドレス',
	'nouvelle_adresse' => '新しい住所',
	'numero' => '電話番号',
	'numeros' => '電話番号',

	// P
	'placeholder_complement_adresse' => '棟、階...',
	'placeholder_email' => 'email@site.com',
	'placeholder_titre_adresse' => '住所を特定する名前',
	'placeholder_titre_email' => 'メールアドレスを特定する名前', # MODIF
	'placeholder_titre_numero' => '電話番号を特定する名前', # MODIF

	// S
	'supprimer_adresse' => 'この住所を削除する',
	'supprimer_email' => 'このメールアドレスを削除する',
	'supprimer_numero' => 'この電話番号を削除する',

	// T
	'titre_coordonnees' => '連絡先',
	'type_adr_dom' => '居住用',
	'type_adr_home' => '個人',
	'type_adr_intl' => '海外',
	'type_adr_parcel' => '区画',
	'type_adr_postal' => '局留め郵便',
	'type_adr_pref' => '主の', # MODIF
	'type_adr_work' => '勤務先',
	'type_coordonnees_url_home' => '住所',
	'type_email_home' => '個人用',
	'type_email_internet' => 'インターネット',
	'type_email_intl' => '国際',
	'type_email_pref' => '優先',
	'type_email_work' => '仕事の',
	'type_email_x400' => 'X.400',
	'type_mel_home' => '個人の',
	'type_mel_intl' => '国際的な',
	'type_mel_work' => '仕事の',
	'type_tel_bbs' => '留守番電話サービス',
	'type_tel_car' => '自動車',
	'type_tel_cell' => '携帯',
	'type_tel_dsl' => 'DSL box',
	'type_tel_fax' => 'ファックス',
	'type_tel_home' => '自宅',
	'type_tel_isdn' => 'ISDN',
	'type_tel_modem' => 'MoDem',
	'type_tel_msg' => '留守番電話',
	'type_tel_pager' => 'ポケベル',
	'type_tel_pcs' => 'PCS',
	'type_tel_pref' => '優先', # MODIF
	'type_tel_text' => 'SMS',
	'type_tel_textphone' => '聴覚障害者用通信機器',
	'type_tel_video' => 'Visiophone',
	'type_tel_voice' => '音声',
	'type_tel_work' => '勤務先'
);
