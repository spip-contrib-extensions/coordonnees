<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/coordonnees?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Dirección',
	'adresse_champ_code_postal_eircode_label' => 'Código',
	'adresse_champ_code_postal_pin_label' => 'PIN',
	'adresse_champ_code_postal_zip_label' => 'ZIP',
	'adresse_champ_localite_dependante_district_label' => 'Distrito',
	'adresse_champ_localite_dependante_neighborhood_label' => 'Barrio',
	'adresse_champ_localite_dependante_suburb_label' => 'Comuna',
	'adresse_champ_localite_dependante_townland_label' => 'Ciudad',
	'adresse_champ_localite_dependante_village_township_label' => 'Comuna',
	'adresse_champ_ville_district_label' => 'Distrito',
	'adresse_champ_ville_post_town_label' => 'Comuna',
	'adresse_champ_ville_suburb_label' => 'Comuna',
	'adresse_champ_zone_administrative_area_label' => 'Zona',
	'adresse_champ_zone_administrative_canton_label' => 'Cantón',
	'adresse_champ_zone_administrative_county_label' => 'Condado',
	'adresse_champ_zone_administrative_department_label' => 'Provincia',
	'adresse_champ_zone_administrative_district_label' => 'Distrito',
	'adresse_champ_zone_administrative_do_si_label' => 'Do Si',
	'adresse_champ_zone_administrative_emirate_label' => 'Emirato',
	'adresse_champ_zone_administrative_island_label' => 'Isla',
	'adresse_champ_zone_administrative_oblast_label' => 'Región',
	'adresse_champ_zone_administrative_parish_label' => 'Parroquia',
	'adresse_champ_zone_administrative_prefecture_label' => 'Prefectura',
	'adresse_champ_zone_administrative_province_label' => 'Provincia',
	'adresse_champ_zone_administrative_state_label' => 'Estado',
	'adresse_perso' => 'Domicilio',
	'adresse_pro' => 'Profesional',
	'adresses' => 'Direcciones',
	'ajouter_adresse' => 'Agregar una dirección',
	'ajouter_email' => 'Agregar e-mail', # MODIF
	'ajouter_numero' => 'Agregar un número', # MODIF
	'ajouter_telephone' => 'Agregar un número', # MODIF

	// B
	'bouton_dissocier' => 'Retirar',
	'bouton_dissocier_adresse' => 'Retirar esta dirección',
	'bouton_dissocier_email' => 'Retirar este e-mail', # MODIF
	'bouton_dissocier_numero' => 'Retirar este número',

	// C
	'configuration_adresses_champs_superflus_explication' => 'Permite no utilizar algunos campos inútiles para este sitio en las durecciones.',
	'configuration_adresses_champs_superflus_label' => 'Campos superfluos de direcciones',
	'configuration_coordonnees' => 'Configuración de la coordinadas',
	'confirmer_suppression_adresse' => '¿Quiere usted realmente supriumir esta dirección?',
	'confirmer_suppression_email' => '¿Quiere usted realmente supriumir este e-mail?',
	'confirmer_suppression_numero' => '¿Quiere usted realmente supriumir este número?', # MODIF
	'contacter_adresse' => 'Contactar por correo',
	'contacter_adresse_qui' => 'Contactar @nom@ por correo',
	'contacter_email' => 'Contactar por e-mail', # MODIF
	'contacter_email_qui' => 'Contactar @nom@ por e-mail', # MODIF
	'contacter_telephone' => 'Contactar por teléfono',
	'contacter_telephone_qui' => 'Contactar @nom@ por teléfono',

	// E
	'editer_adresse' => 'Editar una dirección',
	'editer_email' => 'Editar un e-mail',
	'editer_numero' => 'Editar un número',
	'email' => 'e-mail',
	'emails' => 'E-mail',
	'explication_objets_actifs' => '¿Sobre que objetos editoriales prioponer las coordenadas?',
	'explication_type_email' => 'El tipo puede ser personal o profesional',

	// I
	'info_1_adresse' => '1 dirección',
	'info_1_email' => '1 e-mail',
	'info_1_numero' => '1 número', # MODIF
	'info_aucun_email' => 'Ningún e-mail',
	'info_aucun_numero' => 'Ningún número', # MODIF
	'info_aucune_adresse' => 'Ninguna dirección',
	'info_gauche_numero_adresse' => 'Dirección Nº',
	'info_gauche_numero_email' => 'E-mail Nº', # MODIF
	'info_gauche_numero_numero' => 'Número Nº', # MODIF
	'info_nb_adresses' => '@nb@ direcciones',
	'info_nb_emails' => '@nb@ e-mails',
	'info_nb_numeros' => '@nb@ números', # MODIF
	'item_nouveau_numero' => 'Nuevo número', # MODIF
	'item_nouvel_email' => 'Nuevo e-mail',
	'item_nouvelle_adresse' => 'Nueva dirección',

	// L
	'label_boite_postale' => 'Casilla de correo',
	'label_code_postal' => 'Código postal',
	'label_complement' => 'Complemento',
	'label_email' => 'E-mail',
	'label_localite_dependante' => 'Localidad dependiente',
	'label_numero' => 'Número',
	'label_objets_actifs' => 'Objetos',
	'label_pays' => 'País',
	'label_telephone' => 'Teléfono',
	'label_titre' => 'Título',
	'label_type' => 'Tipo',
	'label_type_adresse' => 'Tipo de dirección',
	'label_type_email' => 'Tipo de e-mail',
	'label_type_numero' => 'Tipo de número',
	'label_ville' => 'Ciudad',
	'label_voie' => 'N<sup>o</sup> & vía',
	'label_zone_administrative' => 'Zona administrativa',
	'logo_adresse' => 'Logo de la dirección',
	'logo_email' => 'Logo del e-mail',
	'logo_numero' => 'Logo del número', # MODIF

	// M
	'modifier_adresse' => 'Modificar esta dirección',
	'modifier_email' => 'Modificar este e-mail',
	'modifier_numero' => 'Modificar este número',

	// N
	'nouveau_numero' => 'Nuevo número',
	'nouvel_email' => 'Nuevo e-mail',
	'nouvelle_adresse' => 'Nueva dirección',
	'numero' => 'Número',
	'numeros' => 'Número',

	// P
	'placeholder_complement_adresse' => 'Piso, número del edificio',
	'placeholder_email' => 'e-mail@site.com',
	'placeholder_titre_adresse' => 'Un nombre para identificar la dirección',
	'placeholder_titre_email' => 'Un nombre para identificar el e-mail', # MODIF
	'placeholder_titre_numero' => 'Un nombre para identificar el número de teléfono', # MODIF

	// S
	'supprimer_adresse' => 'Suprimir esta dirección',
	'supprimer_email' => 'Suprimir este e-mail',
	'supprimer_numero' => 'Suprimir este número',

	// T
	'titre_coordonnees' => 'Coordenadas',
	'type_adr_dom' => 'Residencial',
	'type_adr_home' => 'Personal',
	'type_adr_intl' => 'Extranjero',
	'type_adr_parcel' => 'Parcela',
	'type_adr_postal' => 'Correo (retiro en local de correo) ',
	'type_adr_pref' => 'Principal', # MODIF
	'type_adr_work' => 'Profesional',
	'type_coordonnees_url_home' => 'Domicilio',
	'type_email_home' => 'Personal',
	'type_email_internet' => 'Internet',
	'type_email_intl' => 'Internacional',
	'type_email_pref' => 'Preferido',
	'type_email_work' => 'Profesional',
	'type_email_x400' => 'X.400',
	'type_mel_home' => 'Personal',
	'type_mel_intl' => 'Internacional',
	'type_mel_work' => 'Profesional',
	'type_tel_bbs' => 'Servicio de mensajería',
	'type_tel_car' => 'Vehículo',
	'type_tel_cell' => 'Celular',
	'type_tel_dsl' => 'buzón DSL',
	'type_tel_fax' => 'Fax',
	'type_tel_home' => 'Residencia',
	'type_tel_isdn' => 'RNIS',
	'type_tel_modem' => 'Modem',
	'type_tel_msg' => 'Respondedor',
	'type_tel_pager' => 'Bipper (bipeur)',
	'type_tel_pcs' => 'Servicio de comunicación personal',
	'type_tel_pref' => 'Favorito', # MODIF
	'type_tel_text' => 'Texto',
	'type_tel_textphone' => 'Retranscriptor de texto',
	'type_tel_video' => 'Videoconferencia',
	'type_tel_voice' => 'Vocal',
	'type_tel_work' => 'Profesional'
);
