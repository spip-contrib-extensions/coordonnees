<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/coordonnees?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'adresse' => 'Address',
	'adresse_champ_code_postal_eircode_label' => 'Eircode',
	'adresse_champ_code_postal_pin_label' => 'PIN',
	'adresse_champ_code_postal_zip_label' => 'ZIP',
	'adresse_champ_localite_dependante_district_label' => 'Dependent District',
	'adresse_champ_localite_dependante_neighborhood_label' => 'Dependent Neighborhood',
	'adresse_champ_localite_dependante_suburb_label' => 'Dependent Suburb',
	'adresse_champ_localite_dependante_townland_label' => 'Dependent Townland',
	'adresse_champ_localite_dependante_village_township_label' => 'Village Township',
	'adresse_champ_ville_district_label' => 'District',
	'adresse_champ_ville_post_town_label' => 'Post Town', # RELIRE
	'adresse_champ_ville_suburb_label' => 'Suburb',
	'adresse_champ_zone_administrative_area_label' => 'Area',
	'adresse_champ_zone_administrative_canton_label' => 'Canton',
	'adresse_champ_zone_administrative_county_label' => 'County',
	'adresse_champ_zone_administrative_department_label' => 'Department',
	'adresse_champ_zone_administrative_district_label' => 'District',
	'adresse_champ_zone_administrative_do_si_label' => 'Do Si',
	'adresse_champ_zone_administrative_emirate_label' => 'Emirate',
	'adresse_champ_zone_administrative_island_label' => 'Island',
	'adresse_champ_zone_administrative_oblast_label' => 'Oblast',
	'adresse_champ_zone_administrative_parish_label' => 'Parish',
	'adresse_champ_zone_administrative_prefecture_label' => 'Prefecture', # RELIRE
	'adresse_champ_zone_administrative_province_label' => 'Province',
	'adresse_champ_zone_administrative_state_label' => 'State',
	'adresse_perso' => 'Home',
	'adresse_pro' => 'Professional',
	'adresses' => 'Addresses',
	'ajouter_adresse' => 'Add an address',
	'ajouter_coordonnee_dropdown' => 'Add an item', # RELIRE
	'ajouter_coordonnees_url' => 'Add a link',
	'ajouter_email' => 'Add an email',
	'ajouter_numero' => 'Add a phone number', # MODIF
	'ajouter_telephone' => 'Add a phone number', # MODIF

	// B
	'bouton_dissocier' => 'Remove',
	'bouton_dissocier_adresse' => 'Remove this address',
	'bouton_dissocier_coordonnees_url' => 'Delete this link',
	'bouton_dissocier_email' => 'Remove this email',
	'bouton_dissocier_numero' => 'Remove this number',

	// C
	'configuration_adresses_champs_superflus_explication' => 'Allows you to disable unnecessary fields in addresses.',
	'configuration_adresses_champs_superflus_label' => 'Unnecessary address fields',
	'configuration_coordonnees' => 'Configuration of Address book',
	'confirmer_suppression_adresse' => 'Are you sure you want to detele this address ?',
	'confirmer_suppression_coordonnees_url' => 'Are you sure you want to remove this link?',
	'confirmer_suppression_email' => 'Are you sure you want to detele this email ?',
	'confirmer_suppression_numero' => 'Are you sure you want to delete this phone number ?',
	'confirmer_supprimer_coordonnees_url' => 'Do you confirm the removal of this link?',
	'contacter_adresse' => 'Contact by mail',
	'contacter_adresse_qui' => 'Contact @nom@ by mail',
	'contacter_email' => 'Contact by email',
	'contacter_email_qui' => 'Contact @nom@ by email',
	'contacter_telephone' => 'Contact by phone',
	'contacter_telephone_qui' => 'Contact @nom@ by phone',
	'coordonnees_url' => 'Link',
	'coordonnees_urls' => 'Links',

	// E
	'editer_adresse' => 'Edit an address',
	'editer_coordonnees_url' => 'Edit a link',
	'editer_email' => 'Edit an email',
	'editer_numero' => 'Edit a phone number',
	'email' => 'Email',
	'emails' => 'Emails',
	'explication_identifiant_coordonnees_url' => 'ID of the account corresponding to the URL. Leave empty to attempt to fill in automatically afterwards.',
	'explication_objets_actifs' => 'On which editorial objects should be provided contact information?',
	'explication_titre_coordonnee' => 'A name to identify this item', # RELIRE
	'explication_type_email' => 'Type can be "personal" or "work"',

	// I
	'info_1_adresse' => '1 address',
	'info_1_coordonnees_url' => '1 link',
	'info_1_email' => '1 email',
	'info_1_numero' => '1 number', # MODIF
	'info_aucun_coordonnees_url' => 'No link',
	'info_aucun_email' => 'No email',
	'info_aucun_numero' => 'No number', # MODIF
	'info_aucune_adresse' => 'No address',
	'info_gauche_numero_adresse' => 'Address id',
	'info_gauche_numero_coordonnees_url' => 'Link Id',
	'info_gauche_numero_email' => 'Email id',
	'info_gauche_numero_numero' => 'Number Id', # MODIF
	'info_nb_adresses' => '@nb@ addresses',
	'info_nb_coordonnees_urls' => '@nb@ links',
	'info_nb_emails' => '@nb@ emails',
	'info_nb_numeros' => '@nb@ numbers', # MODIF
	'info_utilisations' => 'Uses:',
	'item_nouveau_coordonnees_url' => 'New link',
	'item_nouveau_numero' => 'New phone number', # MODIF
	'item_nouvel_email' => 'New email',
	'item_nouvelle_adresse' => 'New address',

	// L
	'label_boite_postale' => 'Postal box',
	'label_code_postal' => 'Postal code',
	'label_complement' => 'Additional information',
	'label_email' => 'Email',
	'label_identifiant_coordonnees_url' => 'Account',
	'label_localite_dependante' => 'Dependent area', # RELIRE
	'label_numero' => 'Phone number',
	'label_objets_actifs' => 'Objects',
	'label_pays' => 'Country',
	'label_telephone' => 'Phone',
	'label_titre' => 'Title',
	'label_type' => 'Type',
	'label_type_adresse' => 'Address type',
	'label_type_coordonnees_url' => 'Link type',
	'label_type_email' => 'Type of email',
	'label_type_numero' => 'Type of phone number',
	'label_url_coordonnees_url' => 'URL',
	'label_ville' => 'City',
	'label_voie' => 'Address',
	'label_zone_administrative' => 'Administrative area',
	'logo_adresse' => 'Address Logo',
	'logo_coordonnees_url' => 'Link logo',
	'logo_email' => 'Mail Logo',
	'logo_numero' => 'Number Logo', # MODIF

	// M
	'modifier_adresse' => 'Edit this address',
	'modifier_coordonnees_url' => 'Edit this link',
	'modifier_email' => 'Edit this email',
	'modifier_numero' => 'Edit this phone number',

	// N
	'nouveau_coordonnees_url' => 'New link',
	'nouveau_numero' => 'New phone number',
	'nouvel_email' => 'New email',
	'nouvelle_adresse' => 'New address',
	'numero' => 'Phone Number',
	'numeros' => 'Phone numbers',

	// P
	'placeholder_complement_adresse' => 'Floor, block number...',
	'placeholder_email' => 'email@website.com',
	'placeholder_titre_adresse' => 'A name to identify the address',
	'placeholder_titre_email' => 'A name to identify the email',
	'placeholder_titre_numero' => 'A name to identify the phone number', # MODIF
	'placeholder_url_coordonnees_url' => 'https://site.com/user, irc ://site.com/#channel, xmpp:user@site.com, etc.', # MODIF

	// S
	'supprimer_adresse' => 'Delete this address',
	'supprimer_coordonnees_url' => 'Delete this link',
	'supprimer_email' => 'Delete this email',
	'supprimer_numero' => 'Delete this phone number',

	// T
	'titre_coordonnees' => 'Address book',
	'type_adr_dom' => 'Residential',
	'type_adr_home' => 'Personal',
	'type_adr_intl' => 'Foreign',
	'type_adr_parcel' => 'Plot',
	'type_adr_postal' => 'Postal (in poste restante)',
	'type_adr_pref' => 'Favorite',
	'type_adr_work' => 'Work',
	'type_coordonnees_url_blog' => 'Blog',
	'type_coordonnees_url_facebook' => 'Facebook',
	'type_coordonnees_url_home' => 'Home',
	'type_coordonnees_url_instagram' => 'Instagram',
	'type_coordonnees_url_linkedin' => 'Linkedin',
	'type_coordonnees_url_mastodon' => 'Mastodon',
	'type_coordonnees_url_other' => 'Other',
	'type_coordonnees_url_peertube' => 'Peertube',
	'type_coordonnees_url_pref' => 'Favorite',
	'type_coordonnees_url_school' => 'School',
	'type_coordonnees_url_seenthis' => 'Seenthis',
	'type_coordonnees_url_sociaux' => 'Social networks',
	'type_coordonnees_url_twitter' => 'Twitter',
	'type_coordonnees_url_vimeo' => 'Vimeo',
	'type_coordonnees_url_work' => 'Work',
	'type_coordonnees_url_youtube' => 'Youtube',
	'type_email_home' => 'Personal',
	'type_email_internet' => 'Internet',
	'type_email_intl' => 'International',
	'type_email_pref' => 'Favorite',
	'type_email_work' => 'Work',
	'type_email_x400' => 'X.400',
	'type_mel_home' => 'Personal',
	'type_mel_intl' => 'International',
	'type_mel_work' => 'Work',
	'type_tel_bbs' => 'Messaging service',
	'type_tel_car' => 'Car',
	'type_tel_cell' => 'Mobile',
	'type_tel_dsl' => 'DSL box',
	'type_tel_fax' => 'Telefax',
	'type_tel_home' => 'Residence',
	'type_tel_isdn' => 'ISDN',
	'type_tel_modem' => 'Modem',
	'type_tel_msg' => 'Voicemail',
	'type_tel_pager' => 'Pager',
	'type_tel_pcs' => 'Personal Communications Service',
	'type_tel_pref' => 'Favorite',
	'type_tel_text' => 'SMS',
	'type_tel_textphone' => 'Text transcription',
	'type_tel_video' => 'Video conference  ',
	'type_tel_voice' => 'Vocal',
	'type_tel_work' => 'Work'
);
