<?php

/**
 * Plugin Coordonnées
 * Licence GPL (c) 2010 Matthieu Marcillaud
**/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_objet');

/**
 * [Dépréciée] Insérer un nouveau numéro
 * 
 * @deprecated
 * @uses objet_inserer
 */
function insert_numero($c = '') {
	$champs = [
		'numero' => _T('coordonnees:item_nouveau_numero')
	];
	$id_numero = objet_inserer('numero', null, $champs);
	if (!empty($c) and !empty($c['objet']) and !empty($c['id_objet'])) {
		if (empty($c['type'])) {
			$c['type'] = '';
		}
		$c['id_numero'] = $id_numero;
		sql_insertq("spip_numeros_liens", $c);
	}
	return $id_numero;

}

/**
 * [Dépréciée] Enregistrer certaines modifications d'un numéro
 * 
 * @deprecated
 * @uses objet_modifier
 */
function revisions_numeros($id_numero, $c = false) {
	return objet_modifier('numero', $id_numero, $c);
}