<?php
/**
 * Action : supprimer une adresse web
 *
 * @plugin     Coordonnees
 * @copyright  2014
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Action
 */

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/**
 * Supprimer un coordonnees_url
 *
 * @example
 *   ```
 *   [(#AUTORISER{supprimer, coordonnees_url, #ID_COORDONNEES_URL}|oui)
 *      [(#BOUTON_ACTION{
 *        [(#CHEMIN_IMAGE{coordonnees_url-del-24.svg}|balise_img{<:coordonnees:supprimer_coordonnees_url:/>}|concat{' ',#VAL{<:coordonnees:supprimer_coordonnees_url:/>}|wrap{<b>}}|trim)],
 *        #URL_ACTION_AUTEUR{supprimer_coordonnees_url, #ID_COORDONNEES_URL, #URL_ECRIRE{coordonnees_urls}},
 *        danger coordonnees_url-del-24,
 *        <:coordonnees:confirmer_supprimer_coordonnees_url:/>
 *      })]
 *   ]
 *   ```
 *
 * @example
 *   ```
 *   if (autoriser('supprimer', 'coordonnees_url', $id_coordonnees_url)) {
 *     $supprimer_coordonnees_url = charger_fonction('supprimer_coordonnees_url', 'action');
 *     $supprimer_coordonnees_url($id_coordonnees_url);
 *   }
 *   ```
 *
 * @param string|null $arg
 *   Identifiant à supprimer.
 *   En absence de id utilise l'argument de l'action sécurisée.
 */

function action_supprimer_coordonnees_url_dist(?string $arg = null) {
	$need_confirm = false;

	// Si $arg n'est pas donné directement, le récupérer via _POST ou _GET
	// Dans ce cas on demande forcément confirmation
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		$need_confirm = true;
	}
	$id_coordonnees_url = (int) $arg;

	// Demande de confirmation
	if ($need_confirm) {
		$ok = confirmer_supprimer_coordonnees_url_avant_action(_T('coordonnees:confirmer_suppression_coordonnees_url'), _T('item_oui') . '! ' . _T('coordonnees:supprimer_coordonnees_url'));
	}

	if (
		$id_coordonnees_url > 0
		and autoriser('supprimer', 'coordonnees_url', $id_coordonnees_url)
	) {
		// Loguer la suppression
		$objet = sql_fetsel('*', 'spip_coordonnees_urls', "id_coordonnees_url = $id_coordonnees_url");
		$qui = (!empty($GLOBALS['visiteur_session']['id_auteur']) ? 'auteur #' . $GLOBALS['visiteur_session']['id_auteur'] : 'IP ' . $GLOBALS['ip']);
		spip_log("SUPPRESSION coordonnees_url #$id_coordonnees_url par $qui : " . json_encode($objet), "suppressions" . _LOG_INFO_IMPORTANTE);

		// Supprimer l'objet et ses liens
		sql_delete('spip_coordonnees_urls', "id_coordonnees_url = $id_coordonnees_url");
		sql_delete('spip_coordonnees_urls_liens', "id_coordonnees_url = $id_coordonnees_url");

		// Supprimer les logos
		include_spip('action/editer_logo');
		logo_supprimer('spip_coordonnees_urls', $id_coordonnees_url, 'on');
		logo_supprimer('spip_coordonnees_urls', $id_coordonnees_url, 'off');

		// Invalider le cache
		include_spip('inc/invalideur');
		suivre_invalideur("id='id_coordonnees_url/$id_coordonnees_url'");
	} else {
		spip_log("action_supprimer_coordonnees_url : arg `$arg` pas compris ou non autorisé", 'coordonnees');
	}
}

/**
 * Confirmer avant suppression si on arrive par un bouton action
 *
 * @param string $titre
 * @param string $titre_bouton
 * @param string|null $url_action
 * @return bool
 */
function confirmer_supprimer_coordonnees_url_avant_action($titre, $titre_bouton, $url_action = null) {
	if (!$url_action) {
		$url_action = self();
		$action = _request('action');
		$url_action = parametre_url($url_action, 'action', $action, '&');
	}
	else {
		$action = parametre_url($url_action, 'action');
	}
	$arg = parametre_url($url_action, 'arg');
	$confirm = md5("$action:$arg:".realpath(__FILE__));
	if (_request('confirm_action') === $confirm) {
		return true;
	}
	$url_confirm = parametre_url($url_action, "confirm_action", $confirm, '&');
	include_spip("inc/filtres");
	$bouton_action = bouton_action($titre_bouton, $url_confirm);
	$corps = "<div style='text-align:center;'>$bouton_action</div>";
	include_spip("inc/minipres");
	echo minipres($titre,$corps);
	exit;
}