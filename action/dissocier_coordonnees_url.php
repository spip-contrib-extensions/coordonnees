<?php
/**
 * Action du plugin Coordonnée : dissocier une adresse web d'un objet
 *
 * @plugin     Coordonnées
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Coordonnees\Action
 */

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Dissocier un site d'un objet editorial
 *
 * @example
 *   ```
 *   #URL_ACTION_AUTEUR{dissocier_coordonnees_url, #ID_COORDONNEE_URL/#OBJET/#ID_OBJET, #SELF}
 *   ```
 *
 * @param string|null $arg
 *   Arguments séparés par un charactère non alphanumérique
 *
 *   - id_coordonnees_url : identifiant d'une coordonnée
 *   - objet : type d'objet à dissocier
 *   - id_objet : identifiant de l'objet à dissocier
 */
function action_dissocier_coordonnees_url_dist(?string $arg = null) {

	// Si $arg n'est pas donné directement, le récupérer via _POST ou _GET
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (
		[$id_coordonnees_url, $objet, $id_objet] = preg_split('/\W/', $arg) ?: []
		and (int) $id_coordonnees_url = $id_coordonnees_url
		and (int) $id_objet = $id_objet
		and autoriser('modifier', $objet, $id_objet)
	) {
		include_spip('action/editer_liens');
		objet_dissocier(['coordonnees_url' => $id_coordonnees_url], [$objet => $id_objet]);
	} else {
		spip_log("action_dissocier_coordonnees_url : arg `$arg` pas compris ou non autorisé", 'coordonnees');
	}
}

