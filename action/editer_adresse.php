<?php

/**
 * Plugin Coordonnées
 * Licence GPL (c) 2010 Matthieu Marcillaud
**/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_objet');

/**
 * [Dépréciée] Insérer une nouvelle adresse
 * 
 * @deprecated
 * @uses objet_inserer
 */
function insert_adresse($c = '') {
	$champs = [
		'voie' => _T('coordonnees:item_nouvelle_adresse')
	];
	$id_adresse = objet_inserer('adresse', null, $champs);
	if (!empty($c) and !empty($c['objet']) and !empty($c['id_objet'])) {
		if (empty($c['type'])) {
			$c['type'] = '';
		}
		$c['id_adresse'] = $id_adresse;
		sql_insertq("spip_adresses_liens", $c);
	}
	return $id_adresse;
}

/**
 * [Dépréciée] Enregistrer certaines modifications d'une adresse
 * 
 * @deprecated
 * @uses objet_modifier
 */
function revisions_adresses($id_adresse, $c = false) {
	return objet_modifier('adresse', $id_adresse, $c);
}