<?php
/**
 * Action du plugin Coordonnée : associer une adresse web à un objet
 *
 * @plugin     Coordonnée
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Coordonnees\Action
 */

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * Associer un site à un objet editorial
 *
 * @example
 *   ```
 *   #URL_ACTION_AUTEUR{associer_coordonnees_url, #ID_COORDONNEES_URL/#OBJET/#ID_OBJET, #SELF}
 *   ```
 *
 * @param string|null $arg
 *   arguments séparés par un charactère non alphanumérique
 *
 *   - id_coordonnees_url : identifiant de la coordonnée
 *   - objet : type d'objet à associer
 *   - id_objet : identifiant de l'objet à associer
 */
function action_associer_coordonnees_url_dist(?string $arg = null) {

	// Si $arg n'est pas donné directement, le récupérer via _POST ou _GET
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	if (
		[$id_coordonnees_url, $objet, $id_objet] = preg_split('/\W/', $arg) ?: []
		and (int) $id_coordonnees_url = $id_coordonnees_url
		and (int) $id_objet = $id_objet
		and autoriser('modifier', $objet, $id_objet)
	) {
		include_spip('action/editer_liens');
		objet_associer(['coordonnees_url' => $id_coordonnees_url], [$objet => $id_objet]);
	} else {
		spip_log("action_associer_coordonnees_url : arg `$arg` pas compris ou non autorisé", 'coordonnees');
	}
}
