<?php

/**
 * Plugin Coordonnées
 * Licence GPL (c) 2010 Matthieu Marcillaud
**/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_objet');

/**
 * [Dépréciée] Insérer un nouvel email
 * 
 * @deprecated
 * @uses objet_inserer
 */
function insert_email($c = '') {
	$champs = [
		'email' => _T('coordonnees:item_nouvel_email')
	];
	$id_email = objet_inserer('email', null, $champs);

	if (!empty($c) and !empty($c['objet']) and !empty($c['id_objet'])) {
		if (empty($c['type'])) {
			$c['type'] = '';
		}
		$c['id_email'] = $id_email;
		sql_insertq("spip_emails_liens", $c);
	}
	return $id_email;
}

/**
 * [Dépréciée] Enregistrer certaines modifications d'un email
 * 
 * @deprecated
 * @uses objet_modifier
 */
function revisions_emails($id_email, $c = false) {
	return objet_modifier('email', $id_email, $c);
}