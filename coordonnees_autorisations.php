<?php
/**
 * Définit les autorisations du plugin Coordonnees
 *
 * @plugin     Coordonnees
 * @copyright  2013
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Autorisations
 */


if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function coordonnees_autoriser() {}


// --------------
// Objet Adresses

// creer
function autoriser_adresse_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// voir les fiches completes
function autoriser_adresse_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier
function autoriser_adresse_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// supprimer
// uniquement les orphelins
function autoriser_adresse_supprimer_dist($faire, $type, $id, $qui, $opt) {
	include_spip('action/editer_liens');
	return !count(objet_trouver_liens(array('adresse' => $id), '*')) and ($qui['statut'] == '0minirezo' or $qui['restreint']);
}

// associer (lier / delier)
function autoriser_associeradresses_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' or $qui['restreint'];
}


// --------------
// Objet numeros

// creer
function autoriser_numero_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// voir les fiches completes
function autoriser_numero_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier
function autoriser_numero_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// supprimer
// uniquement les orphelins
function autoriser_numero_supprimer_dist($faire, $type, $id, $qui, $opt) {
	include_spip('action/editer_liens');
	return !count(objet_trouver_liens(array('numero' => $id), '*')) and ($qui['statut'] == '0minirezo' or $qui['restreint']);
}

// associer (lier / delier)
function autoriser_associernumeros_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' or $qui['restreint'];
}


// ------------
// Objet emails

// creer
function autoriser_email_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// voir les fiches completes
function autoriser_email_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// modifier
function autoriser_email_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], array('0minirezo', '1comite'));
}

// supprimer
// uniquement les orphelins
function autoriser_email_supprimer_dist($faire, $type, $id, $qui, $opt) {
	include_spip('action/editer_liens');
	return !count(objet_trouver_liens(array('emails' => $id), '*')) and ($qui['statut'] == '0minirezo' or $qui['restreint']);
}


// associer (lier / delier)
function autoriser_associeremails_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' or $qui['restreint'];
}


// --------------
// Objet adresses web

// Créer
// Admins et rédacteurs
function autoriser_coordonnees_url_creer_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = in_array($qui['statut'], ['0minirezo', '1comite']);
	return $autoriser;
}

// Voir les fiches complètes
// Tout le monde
function autoriser_coordonnees_url_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

// Modifier
// Admins et rédacteurs
function autoriser_coordonnees_url_modifier_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = in_array($qui['statut'], ['0minirezo', '1comite']);
	return $autoriser;
}

// Supprimer
// Uniquement les orphelins, uniquement les admins complets
function autoriser_coordonnees_url_supprimer_dist($faire, $type, $id, $qui, $opt) {
	include_spip('action/editer_liens');
	$autoriser = (
		!count(objet_trouver_liens(['coordonnee_url' => $id], '*'))
		and ($qui['statut'] === '0minirezo' and !$qui['restreint'])
	);
	return $autoriser;
}

// Associer (lier / delier)
// Tous les admins
function autoriser_associercoordonnees_urls_dist($faire, $type, $id, $qui, $opt) {
	$autoriser = ($qui['statut'] === '0minirezo' or $qui['restreint']);
	return $autoriser;
}