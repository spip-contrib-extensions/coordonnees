# Changelog

## Unreleased

## 4.3.1 - 2024-08-15

- #39 Fix Fatal error sur array_key_exists de coordonnees_extraire_identifiant_depuis_url()

## 4.2.4 - 2023-12-07

- #36 Fix installation des tables coordonnees_urls

## 4.2.3 - 2023-08-29

### Fixed

- #32 Éviter une erreur si un adresse n’a pas le champ pays complété, sur la vue des adresses dans l’espace privé

## 4.2.0 - 2023-03-29

### Added

- Gestion des coordonnées liens (sites internet, messageries, etc.) : nouvel objet `coordonnees_url` intitulé `Lien` (#21)
- Balise `#TYPE_COORDONNEE` qui donne le label d'un type de coordonnée (#21)
- Saisie `type_coordonnee` pour choisir le type d'une coordonnée (#21)

### Deprecated

- Fonctions listing des types de coordonnées : `filtre_coordonnees_lister_types_adresses()`, `filtre_coordonnees_lister_types_emails()`, `filtre_coordonnees_lister_types_numeros()` (#21)
- Saisies types de coordonnées : `type_adr`, `type_adresse`, `type_email`, `type_mel`, `type_numero`, `type_tel` (#21)
