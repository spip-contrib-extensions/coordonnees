<?php
/**
 * Pipelines du plugin Coordonnees
 *
 * @plugin     Coordonnees
 * @copyright  2013
 * @author     Marcimat / Ateliers CYM
 * @licence    GNU/GPL
 * @package    SPIP\Coordonnees\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Ajoute le lien et le type lors de l'insertion, si infos dans le request
 */
function coordonnees_post_insertion($flux) {
	if (
		// Si on insère dans un objet de ce plugin
		in_array($flux['args']['table'], array('spip_adresses', 'spip_emails', 'spip_numeros', 'spip_coordonnees_urls'))
		and include_spip('base/objets')
		and $id_coordonnee = intval($flux['args']['id_objet'])
		and $coordonnee = objet_type($flux['args']['table'])
		// Et que dans l'environnement, on a un objet lié
		and $objet = _request('objet')
		and $id_objet = intval(_request('id_objet'))
	) {
		include_spip('action/editer_liens');

		// Quel type
		$type = _request('type') ? _request('type') : '';

		// On associe la coordonnée avec le bon type
		objet_associer(
			array($coordonnee => $id_coordonnee),
			array($objet => $id_objet),
			array('type' => $type)
		);
	}

	return $flux;
}

/**
 * Intervenir juste avant l'enregistrement des données lors de l'édition d'un objet éditorial
 *
 * - Toutes coordonnées : change le type du lien lors d'une édition d'une coordonnée existante
 * - Adresses postales : …
 * - Adresses web : remplir automatiquement le champ identifiant si laissé vide
 */
function coordonnees_pre_edition($flux) {

	$action = $flux['args']['action'] ?? null;
	$type = $flux['args']['type'] ?? null;
	$id_coordonnee = (int) ($flux['args']['id_objet'] ?? 0);

	// toutes coordonnées : typer le lien
	if (
		// Si objet existant de ce plugin
		in_array($type, array('adresse', 'email', 'numero', 'coordonnees_url'))
		and $id_coordonnee > 0
		and $coordonnee = $flux['args']['type']
		// Et que dans l'environnement, on a un objet lié
		and $objet = _request('objet')
		and $id_objet = intval(_request('id_objet'))
	) {
		include_spip('action/editer_liens');

		// On associe la coordonnée avec le bon type
		objet_qualifier_liens(
			array($coordonnee => $id_coordonnee),
			array($objet => $id_objet),
			array('type' => _request('type'))
		);
	}

	// Adresse postales : si on a demandé à éditer un point géolocalisé avec l'adresse
	if (
		$type === 'adresse'
		and $id_adresse = intval($flux['args']['id_objet'])
		and test_plugin_actif('gis')
		and include_spip('inc/config')
		and lire_config('coordonnees/gis/editer_point')
		and include_spip('action/editer_liens')
		and include_spip('inc/modifier')
		and include_spip('inc/filtres')
		and include_spip('action/editer_objet')
	) {
		// Existait-il un point GIS déjà lié à cette adresse ?
		$liens = objet_trouver_liens(array('gis' => '*'), array('adresse' => $id_adresse));
		$id_gis = isset($liens[0]['id_gis']) ? $liens[0]['id_gis'] : 'new';

		// On prend en priorité les valeurs envoyées explicitement lors d'un appel à objet_modifier(adresse), sinon le _request()
		$champs = collecter_requests(
			objet_info('gis', 'champs_editables'),
			array(),
			$flux['args']['data']
		);

		// Seulement si lat ET lon sont remplis, attention 0 est légitime
		if (is_numeric($champs['lat']) and is_numeric($champs['lon'])) {
			// On rajoute la liaison
			$champs['objet'] = 'adresse';
			$champs['id_objet'] = $id_adresse;

			// Si pas de titre (parce que création ou parce que pas de titre à l'adresse)
			if (!isset($champs['titre']) or !$champs['titre']) {
				include_spip('coordonnees_fonctions');

				// On utilise directement les champs qu'on a sous la main, ce qui évitera une requête SQL et en plus ça marche dès la création (puisque PRE_edition)
				$adresse = collecter_requests(
					objet_info('adresse', 'champs_editables'),
					array(),
					$flux['args']['data']
				);

				$champs['titre'] = str_replace("\n", ', ', coordonnees_adresses_formater($adresse, false));
			}

			// On crée le point
			if ($id_gis == 'new') {
				$id_gis = objet_inserer('gis');
			}

			// On autorise toujours la liaison
			include_spip('inc/autoriser');
			autoriser_exception('lier', 'gis', $id_gis, true);

			// On édite le contenu du point
			$retour = objet_modifier('gis', $id_gis, $champs);
		}
		// Sinon c'est qu'on ne veut plus de point pour cette adresse, donc on le supprime et tous ses liens
		// Est-ce trop brutal ? Si on veut juste retirer le point uniquement de l'adresse, il y a l'interface pour délier dans la vue.
		else if ($id_gis = intval($id_gis)) {
			sql_delete('spip_gis', 'id_gis =' . $id_gis);
			sql_delete('spip_gis_liens', 'id_gis =' . $id_gis);
		}
	}

	// Adresses web : auto-remplir parfois le champ identifiant s'il est vide
	if (
		// on édite une adresse web
		$type === 'coordonnees_url'
		// and $action === 'modifier'
		// il n'y a pas d'identifiant
		and empty($flux['data']['identifiant'])
		// mais on a pu en extraire un depuis l'URL
		and $url = ($flux['data']['url'] ?? null)
		and $type_coordonnee = _request('type')
		and include_spip('inc/coordonnees')
		and $identifiant = coordonnees_extraire_identifiant_depuis_url($url, $type_coordonnee)
	) {
		$flux['data']['identifiant'] = $identifiant;
		spip_log("pre_edition : remplir automatiquement l'identifiant $identifiant pour $type $id_coordonnee", 'coordonnees' . _LOG_DEBUG);
	}

	return $flux;
}

/**
 * Si configuré, synchronise les liens d'un point GIS avec ceux d'une adresse
 */
function coordonnees_post_edition_lien($flux) {
	// Si c'est configuré pour synchroniser
	if (
		test_plugin_actif('gis')
		and in_array($flux['args']['objet_source'], array('adresse', 'gis'))
		and include_spip('inc/config')
		and lire_config('coordonnees/gis/synchroniser')
	) {
		// Premier cas : on fait un lien entre une adresse qui a déjà un point, et n'importe quel contenu : ce contenu doit recevoir le point aussi
		if (
			// Si c'est pour un lien d'une adresse sur un contenu
			$flux['args']['objet_source'] == 'adresse'
			and $id_adresse = intval($flux['args']['id_objet_source'])
			// Et que cette adresse a déjà un point GIS lié
			and $liens = objet_trouver_liens(array('gis'=>'*'), array('adresse'=>$id_adresse))
			and $id_gis = intval($liens[0]['id_gis'])
		)	{
			// Si c'était un ajout, on ajoute le point GIS aussi au même objet
			if ($flux['args']['action'] == 'insert') {
				objet_associer(
					array('gis' => $id_gis),
					array($flux['args']['objet'] => $flux['args']['id_objet'])
				);
			}
			// Si c'était un retrait, on retire le point GIS
			elseif ($flux['args']['action'] == 'delete') {
				objet_dissocier(
					array('gis' => $id_gis),
					array($flux['args']['objet'] => $flux['args']['id_objet'])
				);
			}
		}
		// Deuxième cas : on fait un lien entre un point et une adresse existante : tous les contenus déjà liés à l'adresse doivent recevoir le point aussi
		elseif (
			$flux['args']['objet_source'] == 'gis'
			and $flux['args']['objet'] == 'adresse'
			and $id_gis = intval($flux['args']['id_objet_source'])
			and $id_adresse = intval($flux['args']['id_objet'])
			and $liens = objet_trouver_liens(array('adresse'=>$id_adresse), array('*'=>'*'))
			and is_array($liens)
		) {
			// Pour tous les liens trouvés avec l'adresse
			foreach ($liens as $lien) {
				// Si c'était un ajout, on associe le nouveau point GIS à chacun
				if ($flux['args']['action'] == 'insert') {
					objet_associer(
						array('gis' => $id_gis),
						array($lien['objet'] => $lien['id_objet'])
					);
				}
				// Si c'était un retrait, on retire le point GIS à chacun
				elseif ($flux['args']['action'] == 'delete') {
					objet_dissocier(
						array('gis' => $id_gis),
						array($lien['objet'] => $lien['id_objet'])
					);
				}
			}
		}
	}

	return $flux;
}

/*
 * function coordonnees_ieconfig_metas
 *
 * export de configuration avec le plugin ieconfig
 *
 * @param $table
 */
function coordonnees_ieconfig_metas($table) {
	$table['coordonnees']['titre'] = _T('paquet-coordonnees:coordonnees_nom');
	$table['coordonnees']['icone'] = 'prive/themes/spip/images/coordonnees-16.svg';
	$table['coordonnees']['metas_serialize'] = 'contacts_et_organisations';

	return $table;
}
/**
 * Affichage des coordonnées (adresses, mails, numéros)
 * sur la page de visualisation des objets associes
 * Surcharge possible avec 'prive/squelettes/contenu/coordonnees_fiche_nom-objet-associe.html'
**/
function coordonnees_afficher_complement_objet($flux) {
	$texte = '';
	$exec = isset($flux['args']['exec']) ? $flux['args']['exec'] : _request('exec');
	$e = trouver_objet_exec($exec);
	$type = $flux['args']['type'];

	$id_objet = $flux['args']['id'];
	$editable = (in_array(table_objet_sql($type), lire_config('coordonnees/objets', array())) ? 1 : 0);
	$has = false;
	if (!$editable) {
		if (sql_countsel('spip_adresses_liens', 'objet='.sql_quote($type).' AND id_objet='.intval($id_objet))
		  or sql_countsel('spip_numeros_liens', 'objet='.sql_quote($type).' AND id_objet='.intval($id_objet))
		  or sql_countsel('spip_emails_liens', 'objet='.sql_quote($type).' AND id_objet='.intval($id_objet))
		) {
			$has = true;
		}
	}

	$coordonnees_fiche_objet = 'prive/squelettes/contenu/coordonnees_fiche_'.$type;
	if(!find_in_path($coordonnees_fiche_objet.'.html')){
			$coordonnees_fiche_objet = 'prive/squelettes/contenu/coordonnees_fiche_objet';
	}

	if (isset($e['edition']) and !$e['edition'] and ($editable or $has)) {
		$texte .= recuperer_fond(
			$coordonnees_fiche_objet,
			array(
			'objet' => $type,
			'id_objet' => intval($flux['args']['id']),
			'editable' => $editable,
			),
			array('ajax'=>'coordonnees')
		);
	}

	if ($texte) {
		if ($p=strpos($flux['data'], '<!--afficher_fiche_objet-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}


/**
 * Liaisons avec les objets
 * sur la page de visualisation des coordonnées
**/
function coordonnees_affiche_gauche($flux) {
	$texte = '';
	$exec = isset($flux['args']['exec']) ? $flux['args']['exec'] : _request('exec');
	$e = trouver_objet_exec($exec);
	if (is_array($e)
		and !$e['edition']
		and $type = $e['type']
		and in_array($type, array('adresse','email','numero','coordonnees_url'))
		and $id_coordonnee = $flux['args']["id_{$type}"]
	) {
		$texte .= recuperer_fond(
			"prive/squelettes/contenu/utilisations_{$type}",
			array(
			"id_{$type}" => intval($id_coordonnee)
			),
			array('ajax'=>true)
		);
	}

	if ($texte) {
		$flux['data'] .= $texte;
	}

	return $flux;
}


/**
 * Optimiser la base de donnees en supprimant les liens orphelins
 * de l'objet vers quelqu'un et de quelqu'un vers l'objet.
 *
 * @param int $n
 * @return int
 */
function coordonnees_optimiser_base_disparus($flux) {
	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(array('adresse'=>'*', 'numero'=>'*', 'email'=>'*'), '*');
	return $flux;
}

/**
 * Permettre aux JS de savoir si on est dans l'espace privé
 */
function coordonnees_header_prive($flux) {
	$flux = '<script type="text/javascript">var spip_ecrire = true;</script>' . $flux;

	return $flux;
}

/**
 * Ajouter le JS pour gérer les adresses suivant les pays
 */
function coordonnees_jquery_plugins($scripts) {
	$scripts[] = "javascript/coordonnees_adresses.js";

	return $scripts;
}

/**
 * Modifier les saisies d'adresses chargées si on est dans un formulaire posté et que le pays a changé, car ce ne sont plus les mêmes vérifications à faire
 *
 * @param array $flux
 * @return array
 **/
function coordonnees_formulaire_saisies($flux) {
	// Si on est dans un form posté
	if ($flux['args']['je_suis_poste']) {
		// On récupère les pays qui ont changé par l'API
		if (
			$pays_modifies = _request('coordonnees_noms_pays_modifies')
			and is_array($pays_modifies)
		) {
			include_spip('inc/saisies');

			// Pour chaque pays modifié
			foreach ($pays_modifies as $nom) {
				// On récupère le nouveau pays changé
				$code_pays = saisies_request($nom);

				// On va chercher où se trouve ce champ et se placer à cette endroit du tableau de saisies
				if ($chemin_pays = saisies_chercher($flux['data'], $nom, true)) {
					$cle_pays = array_pop($chemin_pays);
					$bon_endroit = &$flux['data']; // le premier parent du champ pays
					foreach ($chemin_pays as $cle) {
						$bon_endroit = &$bon_endroit[$cle];
					}

					// On va chercher l'identifiant de cette adresse
					$identifiant = $bon_endroit[$cle_pays]['options']['adresse-id'];

					// Le champ de pays est-il obligatoire ?
					$obligatoire = $bon_endroit[$cle_pays]['options']['obligatoire'];

					// On va supprimer tous les champs *qui suivent le champ pays* de la même adresse
					$depassement_pays = false;
					foreach ($bon_endroit as $cle=>$saisie) {
						if ($cle == $cle_pays) {
							$depassement_pays = true;
						}
						elseif ($depassement_pays) {
							// Dès qu'on a trouvé un champ de la même adresse, on le vire
							if (isset($saisie['options']['adresse-id']) and $saisie['options']['adresse-id'] == $identifiant) {
								unset($bon_endroit[$cle]);
							}
							// Dès que ce n'est plus de la même adresse on arrête tout, c'est qu'on a fini
							else {
								break;
							}
						}
					}

					// On génère les champs de saisies propre à ce pays (et donc avec les vérifs pour ce pays)
					$saisies_pays = coordonnees_adresses_saisies_par_pays($code_pays, $obligatoire);

					// Si le name a au moins un crochet
					if ($modele = $nom and strpos($modele, '[') !== false) {
						// On remplace le champ pays par $0
						$modele = str_replace('pays', '$0', $modele);
						// On transforme toutes les saisies avec ce modèle
						$saisies_pays =  saisies_transformer_noms(
							$saisies_pays,
							'/^\w+$/',
							$modele
						);
					}

					// On rajoute de nouveau le hidden, en PHP cette fois,
					// car si ça s'arrête dans verifier() il faut continuer de dire que ce n'est pas le même pays qu'au chargement
					$saisies_pays[] = array(
						'saisie' => 'hidden',
						'options' => array(
							'nom' => 'coordonnees_noms_pays_modifies[]',
							'defaut' => $nom,
							'valeur_forcee' => $nom,
						),
					);

					// On remet l'identifiant
					foreach ($saisies_pays as $cle=>$saisie) {
						$saisies_pays[$cle]['options']['adresse-id'] = $identifiant;
						$saisies_pays[$cle]['options']['attributs'] = 'data-adresse-id="'.$identifiant.'"';
					}

					// On insère ces champs juste après le pays
					array_splice($bon_endroit, $cle_pays+1, 0, $saisies_pays);
				}
			}
		}
	}


	return $flux;
}
