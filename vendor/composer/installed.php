<?php return array(
    'root' => array(
        'name' => 'spip-contrib-extensions/coordonnees',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '4ac0731fed662ce138565935422a3cefb038b4be',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'commerceguys/addressing' => array(
            'pretty_version' => 'v1.4.2',
            'version' => '1.4.2.0',
            'reference' => '406c7b5f0fbe4f6a64155c0fe03b1adb34d01308',
            'type' => 'library',
            'install_path' => __DIR__ . '/../commerceguys/addressing',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/collections' => array(
            'pretty_version' => '1.8.0',
            'version' => '1.8.0.0',
            'reference' => '2b44dd4cbca8b5744327de78bafef5945c7e7b5e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/collections',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'reference' => '0e2a4f1f8cdfc7a92ec3b01c9334898c806b30de',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'spip-contrib-extensions/coordonnees' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '4ac0731fed662ce138565935422a3cefb038b4be',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
